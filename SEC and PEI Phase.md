#Security(SEC) PHASE
- first phase of the Platform Initialization  
- 코드를 최소화 해야하고 어샘블리 어를 사용  

#PEI Foundation
![qqq.png](https://bitbucket.org/repo/jbB69A/images/3322592097-qqq.png)

#What is PEI?
- PEI is the second phase of the Platform Initialization  
- PEI phase의 목표는 memory를 초기화 하고 다른 플랫폼 resource들을 DXE phase로 넘겨주는 것이다.

#Why PEI?
- 많은 BIOS 밴더들이 information을 담는데 다른 IA32 레지스터를 사용한다.예) 저장을 위해 EBP를 사용할지 EBX를 사용할지 모름  
- memory initialization과 basic chipset initialization을 하는데 가장 빠른 방법이다.
- C로 코딩 가능

#PEI OUTLINE
![ww.png](https://bitbucket.org/repo/jbB69A/images/1909182203-ww.png)

#PEI's initial memory
- 매우 작은양의 temporary memory가 필요
- 이를 위해 PEI는 Cache as RAM(CAR)를 사용

#PEI's memory map
![qwerqwer.png](https://bitbucket.org/repo/jbB69A/images/247139007-qwerqwer.png)  
- BFV(Boot Fimware Volume) : PEI code가 저장되어 있음   
- FV(Firmware Volume) : "Firmware Storage, vol.3"에 PI specification이 저장되어 있음    
- T-RAM(Temporary memory) : system memory를 초기화 하기 전에 스택과 데이터 공간이 여기에 위치함

#PEI CORE
- PEI Core는 실행할수 있는 PEI 바이너리로 PEIM을 위치시키고 basic service를 제공함

#PEI Core code
- SEC에서 PEI phase로 넘어갈때 PeiMain이 호출된다.
- \MdeModulePkg\Core\Pei\PeiMain에 위치  
![wwweq.png](https://bitbucket.org/repo/jbB69A/images/3807491781-wwweq.png)  
- *SecCoreData : PEI Core의 실행 환경에 대한 정보가 들어있다. ex) t-RAM의 사이즈와 위치, stack 위치, BFV 위치
- *PplList : PEI Core에 의해 생겨난 PPI descriptor들의 list를 가르킨다. 초기화 phase의 한 부분으로 PEI Foundation은 이 SEC-hosted PPI들을 PPI database에 추가할 것이다. PEI Foundation과 모듈들은 이 PPI에 있는 and/or code를 call 하는 서비스를 활용할 수 있다.
- *data : core의 data 공간을 초기화하는데 사용된 old core data를 포인트 한다. NULL이면, 이것은 첫번째 PEIcore에 들어온 것이다.

#PEI Phase Flow
![qq.png](https://bitbucket.org/repo/jbB69A/images/1717461680-qq.png)  
1. power on이 되고 SEC Phase가 시작된다.  
2. 이 시점에 processor가 reset vector의 위치를 지정하는 코드를 실행하고 BFV는 reset vector가 위치한곳에 생성된다.  
3. T-RAM(temporary memory for call stacks) 또한 SEC Phase동안에 만들어 진다.  
4. PEI Core가 시작 되고 Core services를 실행한다.   
5. Core Dispatcher를 부른다.  
6. Core Dispatcher는 PEIM을 부르기 위해서 FV를 찾는다. PEIM은 PPI를 만든다.  
7. PEIM이 entry point(INIT code로 알려진)를 실행한다.  
8. Core dispatcher는 계속 다음 PEIM을 불러온다. 이때, PPI는 한번보다 더 많이 불릴 수 있다.   
9. PEIM이 실행되고 System memory가 초기화된다.   
10. 마지막 PEIM에 DXE Phase로 전환할 수 있게 하는 DXE IPL이 위치한다.  
11. T-RAM에 있는 stack이 System memory로 옮겨지고 DXE IPL이 DXE Phase를 부른다.  

#PEI Core services
- PPI : PEIM-to-PEIM Interface(PPI)가 PEIM 사이에서 intermodule 역할을 할수 있도록 관리한다. PPI는 T-RAM에 있는 database에서 설치된다.
- Boot Mode : boot mode를 관리한다 ex) s3, s5, normal boot
- HOB : Hand-off Block(HOB)이라는 자료구조를 만든다. HOB은 정보를 다음 phase로 넘겨줄 때 사용된다.
- Firmware Volume : PEIM을 찾기 위해 Firmware Volume안에 있는 the Firmware File Systems(FFS) 도움을 준다.
- PEI Memory : 영구적인 메모리가 발견되기 전과 후에 사용할 수 있는 메모리 관리를 제공한다.
- Status code : 일반적인 progress와 error code를 report하는 것을 제공한다. ex) port 80, serial port for simple text output for debug
- Reset : 시스템을 다시 시작할수 있도록 한다.

#PEI Services' functions
![wqqw.png](https://bitbucket.org/repo/jbB69A/images/2846586904-wqqw.png)

#PEI Module(PEIM)
- PEIM은 PEI안에서 실행할수 있고 chipset과 platform 특성들을 지원할 수 있는 firmware code를 모듈화 한 것이다.
>##Characteristics of a PEIM
>- 실행할수 있는 object이다.
>- Seperately built, uncompressed binary image들이다.
>- FV의 file안에 포함되어있다.
>- 그들을 찾을수 있는 메커니즘이 제공되는 한 다수의 FV안에 존재한다.
>- Excute in Place(XIP) : PEIM은 바로 플래시 장비에 있는 그들의 location으로부터 run 한다(RAM에서 load되지 않음)
>- 다른 PEIM을 위해 PPI를 정의한다.
>- PEIM은 PPI가 필요한 요구사항을 만든다.
#PEIM Layout
The PEIM binary image can be executed in place from its location in the firmware volume. or from a compressed component that will be shadowed after permenant memory has been installed. The excutable section of the PEIM may be  either position-depending or position-independing code. If the executable section of the PEIM is position-depending code, location information must be provided in the PEIM image to allow firmware volumes, store software,  share located image to a different location from where was compiled.  
![wwqew.png](https://bitbucket.org/repo/jbB69A/images/1451320451-wwqew.png)
#PEIM to PEIM Interfaces
- Architectural PPI : PPI의 GUID는 PEI Foundation에 알려지고 PPI는 공통의 interface를 제공한다. ReportStatusCode()와 같은 platform-specific하게 구현 할수 있는 서비스를 제공한다. 
                               *GUID는 boot service 환경에서 service와 structure를 구분짓는 128bit 값을 의미한다.
- Additional PPI : PPI는 상호 운용에 중요하지만 PEI Foundation-dependent에는 중요하지 않다. 그러므로 PPI에 접근할수 있도록 PEI services가 필요하다. ex) InstallPpi(), ReinstallPpi(), LocatePpi(), and NotifyPpi().
- PPI는 모든 접근 가능한 PPI의 list를 가진 PPI database를 통해 접근할 수 있다.  
![qwqq.png](https://bitbucket.org/repo/jbB69A/images/2383997554-qwqq.png)
#PEI Source code
![qqqqqqe.png](https://bitbucket.org/repo/jbB69A/images/3700747796-qqqqqqe.png)
#PEI Core dispatcher
-  PEI dispatcher는 PEIM을 찾고 실행하는 PEI Core의 한 부분이다.
#PEI Dispatcher code location
![qweqweqwq.png](https://bitbucket.org/repo/jbB69A/images/2584282372-qweqweqwq.png)

#PEI HOBs
- Hand-off Block은 platform features와 configuration 또는 data를 나타내는 메모리 안에 있는 자료구조이다. HOBs는 PEI phase에서 생성되고 DXE Phase에서 읽어진다.
#HOB list
- HOB list 는 메모리안에 HOB의 sequential list이다.
# PEI Pre-permenant Memory and Post-permenant Memory
- Pre-permenant Memory : 시스템 메모리를 초기화하는 것을 목적으로 함, boot type을 발견, 아
직 system memory가 없음,Completed PEI Memory Map
- Post-permanant Memory : DXE로 전환하고 HOBs를 준비시키는 것을 목적으로 함, 스택을 위한 메모리와 HOB list를 위한 메모리 환경을 가짐, 이때는 시스템 메모리를 가짐, 또한 excuting in place(XIP)를 FV로부터 함, 첫번째 HOB을 가르키는 포인터를 만듬 (PHIT - PEI Hand-off Information Table)
![wewe.png](https://bitbucket.org/repo/jbB69A/images/1573037326-wewe.png)
#PEI End conditions
1. dispatch할 PEIM이 더이상 없을 때
2. PEI Core가 의존적인 PEIM list를 모드 dispatch 했을때

#PEI transition to DXE
1. PEI phase의 시작에서는 어떤 메모리도 초기화 되지 않는다.
2. PEI Core가 PEIM을 dispatch하고 동시에 HOB list를 초기화한 결과를 기록한다.
3. DXE Initial Program Load(IPL) PEIM이 실행되고 거기에 HOBs list가 존재한다.
4. DXE IPL PEIM이 DXE core에 제어권을 넘겨준다. DXE phase로 넘어가는 정보는 오직 HOB list 뿐이다.
#DXE Initial program load(IPL)
- hard coded address를 보호한다.
- HOB을 위해 가장 큰 physical 메모리를 찾는다 (near the top of memory)
- DXE stack을 top of memory로 부터 할당한다.
- DXE core에게 넘겨줄 HOB list의 FV를 찾는다.
- DXE core를 메모리로 로드하고 HOB를 빌드한다.
- stack을 DXE core로 switch한다.
Call to DXE IPL Code  
![qweqas.png](https://bitbucket.org/repo/jbB69A/images/3558509788-qweqas.png)
#PEI Transition Code
![wwwqqe.png](https://bitbucket.org/repo/jbB69A/images/909419047-wwwqqe.png)
#Alternate boot path
![qqwqw.png](https://bitbucket.org/repo/jbB69A/images/3149185353-qqwqw.png)